__author__ = 'ilja'

import pickle
from sys import argv
from misc import Misc
from os import walk, popen

orig_dir = argv[1]
size = int(argv[2])

files = []
for (dirpath, dirnames, filenames) in walk(orig_dir):
    files.extend(filenames)
    break


for file in files:
    if file == 'data':
        print(orig_dir + ' YES')
        out = popen("rm " + orig_dir + "*.np").read()
        exit()
    else:
        pass

print(orig_dir)

data = Misc.restore_data(orig_dir, size)
with open(orig_dir + 'data', 'wb') as f:
    pickle.dump(data, f)

