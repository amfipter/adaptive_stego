__author__ = 'ilja'

from misc import Misc, Classifier_optimization
from sys import argv
from svmutil import *

orig_dir    = argv[1]
stego_dir   = argv[2]
size        = int(argv[3])
train_size  = int(argv[4])

data_orig = list(Misc.restore_data_universal(orig_dir, size))
data_stego = list(Misc.restore_data_universal(stego_dir, size))

data_x = []
data_y = []

for i in range(size*2):
    if i%2 == 0:
        data_x.append(list(data_orig.pop()))
        data_y.append(2)
    else:
        data_x.append(list(data_stego.pop()))
        data_y.append(1)

model = svm_train(data_y[:train_size], data_x[:train_size], '-h 0 -m 1024 -e 0.00001 -c 500 -t 0')
p_label, p_acc, p_val = svm_predict(data_y[train_size:], data_x[train_size:], model)
print(p_acc)