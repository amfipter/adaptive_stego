__author__ = 'ilja'

from misc import Misc, Classifier_optimization
from sys import argv
from random import shuffle
from sklearn.svm import SVC
from sklearn import cross_validation
import numpy as np

orig_dir  = argv[1]
stego_dir = argv[2]
n         = int(argv[3])
cores     = int(argv[4])

train_files = []
test_files = []

def read_splitted_files(base_dir, n, label):
    for i in range(n):
        files = Misc.restore_data(base_dir + str(i + 1) + '/', 10000)
        shuffle(files)
        if len(train_files) < i + 1:
            train_files.append(list())
        if len(test_files) < i + 1:
            test_files.append(list())
        for c in range(len(files)):
            if c < 500:
                test_files[i].append((label, files[c]))
            else:
                train_files[i].append((label, files[c]))

read_splitted_files(orig_dir, n, 0)
read_splitted_files(stego_dir, n, 1)

for i in range(n):
    shuffle(train_files[i])
    shuffle(test_files[i])



svm_classifiers = []
test1_scores = np.zeros(n)
test2_scores = np.zeros(n)

for i in range(n):
    data_x = []
    data_y = []

    for el in train_files[i]:
        data_x.append(el[1])
        data_y.append(el[0])

    svm = SVC(C=5000, kernel='linear')
    scores = cross_validation.cross_val_score(svm, data_x, data_y, cv=5, n_jobs=cores)
    svm.fit(data_x, data_y)
    print("Dir " + str(i+1))
    print("Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))
    svm_classifiers.append(svm)
    test1_scores[i] = scores.mean()

print('')

for i in range(n):
    data_x = []
    data_y = []

    for el in test_files[i]:
        data_x.append(el[1])
        data_y.append(el[0])

    scores = Misc.model_acc(svm_classifiers[i], data_x, data_y)
    print("Accuracy: %0.2f" % (scores))
    test2_scores[i] = scores

print("Mean:")
print("Train mean acc: %0.2f" % (test1_scores.mean()))
print("Test mean acc: %0.2f" % (test2_scores.mean()))

data_x = []
data_y = []

all_train = []
all_test = []
for train in train_files:
    all_train.extend(train)

for test in test_files:
    all_test.extend(test)

shuffle(all_train)
shuffle(all_test)

train_size = len(train_files[0])
test_size  = len(test_files[0])

for el in all_train:
    data_x.append(el[1])
    data_y.append(el[0])

svm_all = SVC(C=5000, kernel='linear')
scores = cross_validation.cross_val_score(svm_all, data_x[:train_size], data_y[:train_size], cv=5, n_jobs=cores)
svm_all.fit(data_x[:train_size], data_y[:train_size])
print("Train Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))

test_score = []
for i in range(10):
    data_x = []
    data_y = []

    shuffle(all_test)

    for el in all_test:
        data_x.append(el[1])
        data_y.append(el[0])

    test_score.append(Misc.model_acc(svm_all, data_x[:test_size], data_y[:test_size]))

scores = np.array(test_score)
print("Test Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))

