__author__ = 'ilja'

from misc import Misc, Classifier_optimization
from sys import argv
from random import shuffle
from sklearn.svm import SVC
from sklearn import cross_validation
from sklearn.decomposition import PCA, FastICA
from sklearn.feature_selection import SelectKBest, chi2, f_classif
import numpy as np
import pickle

orig_dir    = argv[1]
stego_dir   = argv[2]
size        = int(argv[3])
out_dim     = int(argv[4])
out_orig    = argv[5]
out_stego   = argv[6]
method      = argv[7]

orig_data = []
stego_data = []

for i in range(5):
    orig_data.append(list(Misc.restore_data_universal(orig_dir + str(i + 1) + '/', size)))
    stego_data.append(list(Misc.restore_data_universal(stego_dir + str(i + 1) + '/', size)))

best_orig = orig_data.pop(0)
best_stego = stego_data.pop(0)

all_orig = best_orig.copy()
all_stego = best_stego.copy()

data_x = []
data_y = []

t_orig  = best_orig.copy()
t_stego = best_stego.copy()

for i in range(min(len(t_orig), len(t_stego))):
    try:
        if i%2 == 0:
            data_x.append(t_orig.pop())
            data_y.append(0)
        else:
            data_x.append(t_stego.pop())
            data_y.append(1)
    except:
        print(i)
        break

model = None

if method == 'pca':
    model = PCA(n_components=out_dim)
elif method == 'ica':
    model = FastICA(max_iter=50000, n_components=out_dim, tol=0.00001)
elif method == 'chi2':
    model = SelectKBest(chi2, k=out_dim)
elif method == 'fclassif':
    model = SelectKBest(f_classif, k=out_dim)
else:
    print("wrong method")
    exit(0)

print(data_x)

model.fit(data_x, data_y)

orig_data_new  = []
stego_data_new = []

orig_data_new.append(model.transform(best_orig))
stego_data_new.append(model.transform(best_stego))

for i in range(4):
    orig_data_new.append(model.transform(orig_data[i]))
    stego_data_new.append(model.transform(stego_data[i]))

j = 0
for i in range(len(orig_data_new)):
    for el in orig_data_new[i]:
        with open(out_orig + str(i + 1) + '/' + str(j) + '.pgm.np', 'wb') as f:
            pickle.dump(np.array(el), f)
        j += 1

j = 0
for i in range(len(stego_data_new)):
    for el in stego_data_new[i]:
        with open(out_stego + str(i + 1) + '/' + str(j) + '.pgm.np', 'wb') as f:
            pickle.dump(np.array(el), f)
        j += 1

