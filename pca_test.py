__author__ = 'ilja'

from misc import Misc, Classifier_optimization
from sys import argv
from random import shuffle
from sklearn.svm import SVC
from sklearn import cross_validation
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt
import numpy as np

orig_dir  = argv[1]
stego_dir = argv[2]
size      = int(argv[3])

data_orig = list(Misc.restore_data(orig_dir, size))
data_stego = list(Misc.restore_data(stego_dir, size))

data_x = []
data_y = []

for i in range(size*2):
    if i%2 == 0:
        data_x.append(data_orig.pop())
        data_y.append(0)
    else:
        data_x.append(data_stego.pop())
        data_y.append(1)

data_x = np.array(data_x)
data_y = np.array(data_y)
print(data_x.shape)
pca = PCA()
# pca.fit(data_x)
# print(pca.score_samples(data_x))
n_data = pca.fit_transform(data_x)
# print(n_data.shape)
print(pca.n_components)
# exit()
# out1 = pca.explained_variance_ratio_
# r = pca.explained_variance_
# print(r)

plt.figure()
# print(len(out1))
plt.plot(n_data[0])
# plt.plot(n_data[1])
# for el in out1:
#     # print(el.size)
#     plt.plot(el, color='red')

# plt.show()
# exit()

pca_y = PCA()
n_data1 = pca_y.fit_transform(data_x, data_y)
# out2 = pca_y.explained_variance_ratio_
# out.sort()
n = n_data -n_data1
print(n)
plt.plot(n_data1[0], color = 'red')

# print(len(out1))

# plt.figure()
# plt.plot(out1, color='red')
# plt.plot(out2, color='green')
plt.show()
