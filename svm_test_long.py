__author__ = 'ilja'

from misc import Misc, Classifier_optimization
from sys import argv
from sklearn.svm import SVC
from sklearn import cross_validation

orig_dir    = argv[1]
stego_dir   = argv[2]
size        = int(argv[3])
cores       = int(argv[4])



data_orig   = list(Misc.restore_data(orig_dir, size))
data_stego  = list(Misc.restore_data(stego_dir, size))

data_x = []
data_y = []


for i in range(size*2):
    if i%2 == 0:
        data_x.append(data_orig.pop())
        data_y.append(0)
    else:
        data_x.append(data_stego.pop())
        data_y.append(1)

svm = SVC(C=20000, kernel='linear')
scores = cross_validation.cross_val_score(svm, data_x, data_y, cv=5, n_jobs=cores)
print("Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))


