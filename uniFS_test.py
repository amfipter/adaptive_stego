__author__ = 'ilja'

from misc import Misc, Classifier_optimization
from sys import argv
from random import shuffle
from sklearn.svm import SVC
from sklearn import cross_validation
from sklearn.decomposition import PCA, FastICA
import matplotlib.pyplot as plt
from sklearn.feature_selection import SelectKBest, chi2, f_classif
import numpy as np

orig_dir  = argv[1]
stego_dir = argv[2]
size      = int(argv[3])

data_orig = list(Misc.restore_data(orig_dir, size))
data_stego = list(Misc.restore_data(stego_dir, size))

data_x = []
data_y = []

for i in range(size*2):
    if i%2 == 0:
        data_x.append(data_orig.pop())
        data_y.append(0)
    else:
        data_x.append(data_stego.pop())
        data_y.append(1)

data_x = np.array(data_x)
data_y = np.array(data_y)

model = SelectKBest(chi2, k=100)
model.fit(data_x, data_y)
n_data = model.transform(data_x)

print(data_x.shape)
print(n_data.shape)
print(n_data[0])
