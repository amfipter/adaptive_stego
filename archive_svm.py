__author__ = 'ilja'

from misc import Misc, Classifier_optimization
from sys import argv
from sklearn.svm import SVC
from sklearn import cross_validation

all_srm        = argv[1]
all_srm_stego  = argv[2]
best_srm       = argv[3]
best_srm_stego = argv[4]
size           = int(argv[5])
cores          = int(argv[6])


data_orig   = list(Misc.restore_data(all_srm, size))
data_stego  = list(Misc.restore_data(all_srm_stego, size))

data_best_orig   = list(Misc.restore_data(best_srm, 2000))
data_best_stego  = list(Misc.restore_data(best_srm_stego, 2000))

data_x = []
data_y = []

data_best_x = []
data_best_y = []

for i in range(size*2):
    if i%2 == 0:
        data_x.append(data_orig.pop())
        data_y.append(0)
    else:
        data_x.append(data_stego.pop())
        data_y.append(1)

for i in range(2000):
    if i%2 == 0:
        data_best_x.append(data_best_orig.pop())
        data_best_y.append(0)
    else:
        data_best_x.append(data_best_stego.pop())
        data_best_y.append(1)

print("Common SVM:")
# svm_all = Classifier_optimization.SVM_opt(data_x, data_y, cores)
svm_all = SVC(C=500, kernel='linear')
scores = cross_validation.cross_val_score(svm_all, data_x, data_y, cv=5, n_jobs=cores)
svm_all.fit(data_x, data_y)
print("Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))

print("Best SVM:")
# svm_best = Classifier_optimization.SVM_opt(data_best_x, data_best_y, cores)
svm_best = SVC(C=500, kernel='linear')
scores = cross_validation.cross_val_score(svm_best, data_best_x, data_best_y, cv=5, n_jobs=cores)
print("Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))


print("Common SVM on best pict:")
print("500:")
print(Misc.model_acc(svm_all, data_best_x[:500], data_best_y[:500]))
print("1000:")
print(Misc.model_acc(svm_all, data_best_x[:1000], data_best_y[:1000]))