__author__ = 'ilja'
from misc import Misc, Classifier_optimization
from sys import argv
from sklearn.svm import SVC
from sklearn import cross_validation

orig_dir    = argv[1]
stego_dir   = argv[2]
# size        = int(argv[3])
cores       = int(argv[3])
start       = int(argv[4])
end         = int(argv[5])
step        = int(argv[6])

# if not cores == 24:
#     print('cores')
#     exit()

data_orig   = list(Misc.restore_data(orig_dir, end + 10))
data_stego  = list(Misc.restore_data(stego_dir, end + 10))

data_x = []
data_y = []

for i in range(end + 2):
    if i%2 == 0:
        data_x.append(data_orig.pop())
        data_y.append(0)
    else:
        data_x.append(data_stego.pop())
        data_y.append(1)

for c in range(start, end, step):
    print(c)

    # if c > 2000:
    #     cores = 16
    # if c > 3000:
    #     cores = 12
    # if c > 4000:
    #     cores = 8
    # if c > 5000:
    #     cores = 5
    # if c > 6000:
    #     cores = 3
    # if c > 7000:
    #     cores = 2
    # if c > 8000:
    #     cores = 1



    # Classifier_optimization.SVM_opt(data_x, data_y, cores)
    svm = SVC(C=3000, kernel='linear')
    scores = cross_validation.cross_val_score(svm, data_x[:c], data_y[:c], cv=5, n_jobs=cores)
    print("Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))
