__author__ = 'amfipter'

from misc import Misc, Classifier_optimization
import sys
import numpy as np
from random import shuffle
from sklearn.preprocessing import normalize, scale
from sklearn import metrics, svm
from sklearn.ensemble import ExtraTreesClassifier, RandomForestClassifier, AdaBoostClassifier, GradientBoostingClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.grid_search import RandomizedSearchCV, GridSearchCV
from scipy.stats import uniform as sp_rand
from sklearn.linear_model import Ridge
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.cross_validation import train_test_split
from sklearn.linear_model import SGDClassifier
# from guppy import hpy

stego_type = sys.argv[1]
cores_count = int(sys.argv[2])

dir_orig = "data_orig/"
dir_hugo = "data_hugo/"
dir_wow  = "data_wow/"
dir_s_uniward = "data_s/"

data_size = 10000

## Read data

data_orig = Misc.read_data(dir_orig, data_size)

data_hugo = []
data_wow  = []
data_s    = []

if stego_type == 'hugo':
    data_hugo = Misc.read_data(dir_hugo, data_size)

if stego_type == 'wow':
    data_wow  = Misc.read_data(dir_wow, data_size)

if stego_type == 'uniward':
    data_s    = Misc.read_data(dir_s_uniward, data_size)

data = []

for d in data_orig:
    data.append((d, 0))

for d in data_hugo:
    data.append((d, 1))

for d in data_wow:
    data.append((d, 2))

for d in data_s:
    data.append((d, 3))

## Shuffle all data

shuffle(data)

data_x = []
data_y = []

hugo_data_x = []
hugo_data_y = []

wow_data_x  = []
wow_data_y  = []

s_data_x    = []
s_data_y    = []

for el in data:
    data_x.append(el[0])
    data_y.append(el[1])
    if (el[1] == 0 or el[1] == 1):
        hugo_data_x.append(el[0])
        hugo_data_y.append(el[1])

    if (el[1] == 0 or el[1] == 2):
        wow_data_x.append(el[0])
        wow_data_y.append(el[1])

    if (el[1] == 0 or el[1] == 3):
        s_data_x.append(el[0])
        s_data_y.append(el[1])

# train_x, test_x, train_y, test_y = train_test_split(data_x, data_y, test_size=0.25)
hugo_train_x, hugo_test_x, hugo_train_y, hugo_test_y = train_test_split(hugo_data_x, hugo_data_y, test_size=0.25)
wow_train_x, wow_test_x, wow_train_y, wow_test_y = train_test_split(wow_data_x, wow_data_y, test_size=0.25)
s_train_x, s_test_x, s_train_y, s_test_y = train_test_split(s_data_x, s_data_y, test_size=0.25)

# train_x = np.matrix(train_x)
# train_y = np.array(train_y)
#
# print(len(train_x))
# print(len(train_y))
# print(len(test_x))
# print(len(test_y))
#
# train_x = normalize(train_x)
# test_x  = normalize(test_x)
#
# train_x = scale(train_x)
# test_x  = scale(test_x)

hugo_train_x = scale(hugo_train_x)
hugo_test_x  = scale(hugo_test_x)
wow_train_x  = scale(wow_train_x)
wow_test_x   = scale(wow_test_x)
s_train_x    = scale(s_train_x)
s_test_x     = scale(s_test_x)

##Normalize unsplitted data
# hugo_data_x  = scale(hugo_data_x)
# hugo_data_y  = scale(hugo_data_y)
# wow_data_x   = scale(wow_data_x)
# wow_data_y   = scale(wow_data_y)
# s_data_x     = scale(s_data_x)
# s_data_y     = scale(s_data_y)

# model = ExtraTreesClassifier()
# model = DecisionTreeClassifier()
# model = GaussianNB()
# model = RandomForestClassifier()
# model = SGDClassifier()
# model = AdaBoostClassifier()
# model = GradientBoostingClassifier()

# Misc.quick_tests(train_x, train_y, test_x, test_y)

if stego_type == 'hugo':
    print("===============HUGO")
    # Misc.quick_tests(hugo_train_x, hugo_train_y, hugo_test_x, hugo_test_y)
    # Misc.voting_all(hugo_data_x, hugo_data_y)
    hugo_optimizer = Classifier_optimization(hugo_train_x, hugo_train_y, hugo_test_x, hugo_test_y, cores_count)
    hugo_optimizer.ET_opt()
    hugo_optimizer.DT_opt()
    hugo_optimizer.RF_opt()
    hugo_optimizer.SGD_opt()

if stego_type == 'wow':
    print("================WOW")
    # Misc.quick_tests(wow_train_x, wow_train_y, wow_test_x, wow_test_y)
    # Misc.voting_all(wow_data_x, wow_data_y)
    wow_optimizer = Classifier_optimization(wow_train_x, wow_train_y, wow_test_x, wow_test_y, cores_count)
    wow_optimizer.ET_opt()
    wow_optimizer.DT_opt()
    wow_optimizer.RF_opt()
    wow_optimizer.SGD_opt()


if stego_type == 'uniward':
    print("==================S")
    # Misc.quick_tests(s_train_x, s_train_y, s_test_x, s_test_y)
    # Misc.voting_all(s_data_x, s_data_y)
    s_optimizer = Classifier_optimization(s_train_x, s_train_y, s_test_x, s_test_y, cores_count)
    s_optimizer.ET_opt()
    s_optimizer.DT_opt()
    s_optimizer.RF_opt()
    s_optimizer.SGD_opt()

# print("--------ADA--------")
# print("HUGO:")
# model = AdaBoostClassifier()
# Misc.test_c(model, hugo_train_x, hugo_train_y, hugo_test_x, hugo_test_y)
#
# print("WOW:")
# model = AdaBoostClassifier()
# Misc.test_c(model, wow_train_x, wow_train_y, wow_test_x, wow_test_y )
#
# print("S:")
# model = AdaBoostClassifier()
# Misc.test_c(model, s_train_x, s_train_y, s_test_x, s_test_y)


# print(train_x)
# print(train_y)
#
# model.fit(train_x, train_y)
# predict = model.predict(test_x)
#
# c = 0
# for i in range(len(predict)):
#     if predict[i] == test_y[i]:
#         c += 1
#
# print(float(c) / len(test_y))
#
# print(metrics.classification_report(test_y, predict))
# print(metrics.confusion_matrix(test_y, predict))



# print(data[0])




