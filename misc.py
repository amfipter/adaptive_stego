__author__ = 'amfipter'

from os import walk, stat, path, popen, chdir
import numpy as np
from sklearn import metrics
from sklearn.ensemble import ExtraTreesClassifier, RandomForestClassifier, AdaBoostClassifier, GradientBoostingClassifier, VotingClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.grid_search import RandomizedSearchCV, GridSearchCV
from scipy.stats import uniform as sp_rand
from sklearn.linear_model import Ridge
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.cross_validation import train_test_split
from sklearn.linear_model import SGDClassifier
from sklearn import cross_validation
from sklearn.svm import SVC
import pickle


class Misc:
    def restore_data_universal(dir, size=1000):
        files = []
        for (dirpath, dirnames, filenames) in walk(dir):
            files.extend(filenames)
            break
        if not files.__contains__('data'):
            return Misc.restore_data(dir, size)

        data = None

        with open(dir + 'data', 'rb') as f:
            data = pickle.load(f)

        return data

    def read_data(dir, size = 1000):
        files = []
        for (dirpath, dirnames, filenames) in walk(dir):
            files.extend(filenames)
            break
        data = []
        i = 0
        for file in files:
            if i >= size:
                break
            f = open(dir + file)
            v_t = np.zeros(34671)
            v = f.read().split(" ")
            j = 0
            for el in v:
                try:
                    v_t[j] = (float(el))
                except:
                    continue
                j += 1
            data.append(v_t)
            i += 1
        return data

    def test_c(model, train_x, train_y, test_x, test_y):
        model.fit(train_x, train_y)
        predict = model.predict(test_x)

        c = 0
        for i in range(len(predict)):
            if predict[i] == test_y[i]:
                c += 1

        print(float(c) / len(test_y))

        print(metrics.classification_report(test_y, predict))
        print(metrics.confusion_matrix(test_y, predict))

    def quick_tests(train_x, train_y, test_x, test_y):
        print("ExtraTreesClassifier")
        model = ExtraTreesClassifier()
        Misc.test_c(model, train_x, train_y, test_x, test_y)

        print("DecisionTreeClassifier")
        model = DecisionTreeClassifier()
        Misc.test_c(model, train_x, train_y, test_x, test_y)

        print("GaussianNB")
        model = GaussianNB()
        Misc.test_c(model, train_x, train_y, test_x, test_y)

        print("RandomForestClassifier")
        model = RandomForestClassifier()
        Misc.test_c(model, train_x, train_y, test_x, test_y)

        print("SGDClassifier")
        model = SGDClassifier()
        Misc.test_c(model, train_x, train_y, test_x, test_y)

        print("KNeighborsClassifier")
        model = KNeighborsClassifier()
        Misc.test_c(model, train_x, train_y, test_x, test_y)

    def voting_all(x, y):
        m1 = ExtraTreesClassifier()
        m2 = DecisionTreeClassifier()
        m3 = GaussianNB()
        m4 = RandomForestClassifier()
        m5 = SGDClassifier()

        model = VotingClassifier(estimators = [('et', m1), ('dt', m2), ('GNB', m3), ('RF', m4), ('SGD', m5)], voting = 'hard')
        scores = cross_validation.cross_val_score(model, x, y, cv=5, scoring='accuracy')
        print("Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std()))

    def restore_data(dir, size):
        files = []
        for (dirpath, dirnames, filenames) in walk(dir):
            files.extend(filenames)
            break
        data = []

        i = 0
        for file in files:
            if i > size:
                break
            with open(dir + file, 'rb') as f:
                # print(file)
                data.append(pickle.load(f))
            i += 1
        return np.array(data)

    def lzma_size(dir, file):
        out = popen("lzma -k -9 " + dir + file).read()
        size = stat(dir + file + '.lzma').st_size
        out = popen('rm ' + dir + file + '.lzma').read()
        return size

    def restore_data_with_names(dir):
        files = []
        for (dirpath, dirnames, filenames) in walk(dir):
            files.extend(filenames)
            break
        data = []

        i = 0
        for file in files:
            f = open(dir + file, 'rb')
            data.append((file, pickle.load(f)))
            f.close()
            i += 1
        return np.array(data)

    def model_acc(model, data_x, data_y):
        y = model.predict(data_x)
        c = 0
        for i in range(len(y)):
            if y[i] == data_y[i]:
                c += 1
        return c / float(len(data_y))




class Classifier_optimization:
    def __init__(self, train_x, train_y, test_x, test_y, cores=6):
        self.train_x = train_x
        self.train_y = train_y
        self.test_x = test_x
        self.test_y = test_y
        self.cores = cores

    def ET_opt(self):
        model = ExtraTreesClassifier()
        param_grid = {'n_estimators' : [5, 10, 20, 40, 80, 160, 300, 500], 'criterion': ('gini', 'entropy'), 'max_features': ('sqrt', 'log2', None), 'min_samples_split': [2, 3, 4] }
        rsearch = GridSearchCV(model, param_grid, n_jobs=self.cores)
        rsearch.fit(self.train_x, self.train_y)

        print(rsearch.best_score_)
        print(rsearch.best_estimator_)

    def DT_opt(self):
        model = DecisionTreeClassifier()
        param_grid = { 'criterion': ('gini', 'entropy'), 'max_features': ('sqrt', 'log2', None) }

        rsearch = GridSearchCV(model, param_grid, n_jobs=self.cores)
        rsearch.fit(self.train_x, self.train_y)

        print(rsearch.best_score_)
        print(rsearch.best_estimator_)

    # def GNB_opt(self):
    #     model = GaussianNB()

    def RF_opt(self):
        model = RandomForestClassifier()
        param_grid = {'n_estimators' : [5, 10, 20, 40, 80, 160, 300, 500], 'criterion': ('gini', 'entropy'), 'max_features': ('sqrt', 'log2', None), 'min_samples_split': [2, 3, 4]}
        rsearch = GridSearchCV(model, param_grid, n_jobs=self.cores)
        rsearch.fit(self.train_x, self.train_y)

        print(rsearch.best_score_)
        print(rsearch.best_estimator_)

    def SGD_opt(self):
        model = SGDClassifier()
        param_grid = { 'loss' : ('hinge', 'log', 'modified_huber', 'squared_hinge', 'perceptron', 'squared_loss', 'huber', 'epsilon_insensitive', 'squared_epsilon_insensitive'), 'penalty' : ('none', 'l2', 'l1', 'elasticnet'), 'n_iter' : [3, 5, 10, 20]}

        rsearch = GridSearchCV(model, param_grid, n_jobs=self.cores)
        rsearch.fit(self.train_x, self.train_y)

        print(rsearch.best_score_)
        print(rsearch.best_estimator_)

    def ADA_opt(self):
        model = AdaBoostClassifier()
        param_grid = { 'base_estimator' : (ExtraTreesClassifier(), DecisionTreeClassifier(), RandomForestClassifier(), SGDClassifier()), 'n_estimators' : [5, 10, 20, 40, 80, 160, 300, 500]}

        rsearch = GridSearchCV(model, param_grid, n_jobs=self.cores)
        rsearch.fit(self.train_x, self.train_y)

        print(rsearch.best_score_)
        print(rsearch.best_estimator_)

    def SVM_opt(data_x, data_y, cores):
        model = SVC(kernel="linear")
        param_grid = {'C' : [3000, 4000, 5000, 6000, 8000, 10000, 20000, 40000], 'shrinking' : (True, False)}
        rsearch = GridSearchCV(model, param_grid, n_jobs=cores, cv=5)
        rsearch.fit(data_x, data_y)

        print(rsearch.best_score_)
        print(rsearch.best_estimator_)
        return rsearch










