__author__ = 'ilja'

from misc import Misc, Classifier_optimization
from sys import argv

orig_dir    = argv[1]
stego_dir   = argv[2]
size        = int(argv[3])
cores       = int(argv[4])
arg         = argv[5]


data_orig   = list(Misc.restore_data(orig_dir, size))
data_stego  = list(Misc.restore_data(stego_dir, size))

data_x = []
data_y = []


for i in range(size*2):
    if i%2 == 0:
        data_x.append(data_orig.pop())
        data_y.append(0)
    else:
        data_x.append(data_stego.pop())
        data_y.append(1)


opt = Classifier_optimization(data_x, data_y, None, None, cores)
opt.ADA_opt()

# Classifier_optimization.SVM_opt(data_x, data_y, cores)
#
# if(arg == 'all'):
#     opt = Classifier_optimization(data_x, data_y, None, None, cores)
#     print("ET")
#     opt.ET_opt()
#     print("DT")
#     opt.DT_opt()
#     print("RF")
#     opt.RF_opt()
#     print("SGC")
#     opt.SGD_opt()
#     print("ADA")
#     opt.ADA_opt()