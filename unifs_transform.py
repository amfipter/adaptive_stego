__author__ = 'ilja'

from misc import Misc, Classifier_optimization
from sys import argv
from random import shuffle
from sklearn.svm import SVC
from sklearn import cross_validation
from sklearn.decomposition import PCA
import numpy as np
import pickle
from sklearn.feature_selection import SelectKBest, chi2, f_classif

orig_dir  = argv[1]
stego_dir = argv[2]
size      = int(argv[3])
out_dim   = int(argv[4])
out_orig  = argv[5]
out_stego = argv[6]
typec     = argv[7]

orig_data = list(Misc.restore_data(orig_dir, size))
stego_data = list(Misc.restore_data(stego_dir, size))

# data = orig_data.copy()
# data.extend(stego_data)

data_x = []
data_y = []

t_orig  = orig_data.copy()
t_stego = stego_data.copy()

for i in range(size*2):
    try:
        if i%2 == 0:
            data_x.append(t_orig.pop())
            data_y.append(0)
        else:
            data_x.append(t_stego.pop())
            data_y.append(1)
    except:
        print(i)
        print(len(data_x))
        print(len(data_y))
        break

data_x = np.array(data_x)
data_y = np.array(data_y)

orig_data = np.array(orig_data)
stego_data = np.array(stego_data)

# n_data = None
orig_data_new = None
stego_data_new = None

if typec == 'chi2':
    model = SelectKBest(chi2, k=out_dim)
    model.fit(data_x, data_y)
    orig_data_new = model.transform(orig_data)
    stego_data_new = model.transform(stego_data)

elif typec == 'fclassif':
    model = SelectKBest(f_classif, k=out_dim)
    model.fit(data_x, data_y)
    orig_data_new = model.transform(orig_data)
    stego_data_new = model.transform(stego_data)
else:
    raise Exception('bad args')

print(data_x.shape)
# print(stego_data.shape)
print(orig_data_new.shape)
print(stego_data_new.shape)

j = 0
for el in orig_data_new:
    with open(out_orig + str(j) + '.pgm.np', 'wb') as f:
        pickle.dump(np.array(el), f)
    j += 1

j = 0
for el in stego_data_new:
    with open(out_stego + str(j) + '.pgm.np', 'wb') as f:
        pickle.dump(np.array(el), f)
    j += 1

