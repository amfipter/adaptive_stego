__author__ = 'ilja'

from misc import Misc, Classifier_optimization
from sys import argv
from random import shuffle
from sklearn.svm import SVC
from sklearn import cross_validation
from sklearn.decomposition import PCA
import numpy as np
import pickle

orig_dir  = argv[1]
stego_dir = argv[2]
size      = int(argv[3])
out_dim   = int(argv[4])
out_orig  = argv[5]
out_stego = argv[6]

orig_data = list(Misc.restore_data(orig_dir, size))
stego_data = list(Misc.restore_data(stego_dir, size))

# data = orig_data.copy()
# data.extend(stego_data)

data_x = []
data_y = []

t_orig  = orig_data.copy()
t_stego = stego_data.copy()

for i in range(size*2):
    try:
        if i%2 == 0:
            data_x.append(t_orig.pop())
            data_y.append(0)
        else:
            data_x.append(t_stego.pop())
            data_y.append(1)
    except:
        print(i)
        break

data_x = np.array(data_x)
data_y = np.array(data_y)

print(data_x.shape)
pca = PCA(n_components=out_dim)
pca.fit(data_x, data_y)

orig_data = np.array(orig_data)
stego_data = np.array(stego_data)

orig_data_new = pca.transform(orig_data)
stego_data_new = pca.transform(stego_data)

j = 0
for el in orig_data_new:
    with open(out_orig + str(j) + '.pgm.np', 'wb') as f:
        pickle.dump(np.array(el), f)
    j += 1

j = 0
for el in stego_data_new:
    with open(out_stego + str(j) + '.pgm.np', 'wb') as f:
        pickle.dump(np.array(el), f)
    j += 1


# out = pca.explained_variance_ratio_
#
# w_ratio = []
#
# for i in range(len(out)):
#     w_ratio.append((i, out[i]))
#
# w_ratio.sort(key=lambda x: x[1], reverse=True)
# w_ratio = w_ratio[:out_dim]
#
# w_orig  = []
# w_stego = []
#
# j = 0
# for el in orig_data:
#     v = []
#     for i in w_ratio:
#         v.append(el[i[0]])
#     # w_orig.append(np.array(v))
#     with open(out_orig + str(j) + '.pgm.np', 'wb') as f:
#         pickle.dump(np.array(v), f)
#     j += 1
#
#
# j = 0
# for el in stego_data:
#     v = []
#     for i in w_ratio:
#         v.append(el[i[0]])
#     # w_stego.append(np.array(v))
#     with open(out_stego + str(j) + '.pgm.np', 'wb') as f:
#         pickle.dump(np.array(v), f)
#     j += 1





