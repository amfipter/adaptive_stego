__author__ = 'ilja'

from misc import Misc, Classifier_optimization
from sys import argv
from random import shuffle
from sklearn.svm import SVC
from sklearn import cross_validation
from sklearn.decomposition import PCA, FastICA, NMF
# import matplotlib.pyplot as plt
import numpy as np

orig_dir  = argv[1]
stego_dir = argv[2]
size      = int(argv[3])

data_orig = list(Misc.restore_data(orig_dir, size))
data_stego = list(Misc.restore_data(stego_dir, size))

data_x = []
data_y = []

for i in range(size*2):
    if i%2 == 0:
        data_x.append(data_orig.pop())
        data_y.append(0)
    else:
        data_x.append(data_stego.pop())
        data_y.append(1)

data_x = np.array(data_x)
data_y = np.array(data_y)


# min = 0
# for i in range(len(data_x)):
#     data_x[i].sort()
#     t = data_x[i][-10:]
#     print(t)

model = NMF()
model.fit(data_x)
n_data = model.transform(data_x)
print(n_data.shape)

